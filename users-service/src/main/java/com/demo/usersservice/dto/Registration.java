package com.demo.usersservice.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.PositiveOrZero;

import java.math.BigDecimal;

public record Registration(
        @NotBlank(message = "Please provide a name!") String name,
//        @Email(message = "Please provide an valid email!") TODO supply valid email regex
        @NotBlank(message = "Please provide an email!") String email,
        @PositiveOrZero(message = "Please provide a positive amount!") BigDecimal openingAmount) {
}
