package com.demo.usersservice.service;

import com.demo.usersservice.dto.Registration;
import com.demo.usersservice.kafka.producers.UserRegisteredProducer;
import com.demo.usersservice.model.User;
import com.demo.usersservice.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@AllArgsConstructor
@Transactional
public class UserService {

    private final UserRepository userRepository;
    private final UserRegisteredProducer producer;

    public Long createUser(Registration registration) {
        if (userRepository.existsByEmailIs(registration.email())) {
            throw new RuntimeException("email already exists"); //TODO custom ex
        }

        final User user = User.builder()
                              .name(registration.name())
                              .email(registration.email())
                              .build();

        userRepository.save(user);
        producer.sendNewUserRegisteredEvent(user, registration.openingAmount());

        return user.getId();
    }
}