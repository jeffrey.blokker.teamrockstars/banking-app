package com.demo.usersservice.kafka.producers;

import com.demo.usersservice.kafka.dto.UserRegisteredEvent;
import com.demo.usersservice.model.User;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

import static com.demo.usersservice.kafka.config.KafkaTopicConfig.USER_REGISTERED_TOPIC;

@Slf4j
@Component
@AllArgsConstructor
public class UserRegisteredProducer {

    private final KafkaTemplate<String, String> kafkaTemplate;

    public void sendNewUserRegisteredEvent(User user, BigDecimal openingAmount) {

        UserRegisteredEvent event = new UserRegisteredEvent(UUID.randomUUID(), user.getId(), openingAmount);
        try {
            kafkaTemplate.send(USER_REGISTERED_TOPIC, new ObjectMapper().writeValueAsString(event)).get();

        } catch (InterruptedException | ExecutionException e) {
            throw new RuntimeException(e);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e); //TODO custom ex
        }
    }
}
