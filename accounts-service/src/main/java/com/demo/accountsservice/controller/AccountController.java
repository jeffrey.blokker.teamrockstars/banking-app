package com.demo.accountsservice.controller;

import com.demo.accountsservice.dto.AccountCashTransfer;
import com.demo.accountsservice.model.Account;
import com.demo.accountsservice.service.AccountService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("api/v1/accounts")
@RequiredArgsConstructor
public class AccountController {

    private final AccountService accountService;

    @GetMapping("/user/{userId}")
    public List<Account> listAccounts(@PathVariable Long userId) {
        return accountService.findAccountsBy(userId);
    }

    @PostMapping("/transfer")
    public void transfer(@RequestBody AccountCashTransfer transfer) {
        accountService.transferCashBetweenAccounts(transfer);
    }
}
