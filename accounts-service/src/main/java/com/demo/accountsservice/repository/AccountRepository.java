package com.demo.accountsservice.repository;

import com.demo.accountsservice.model.Account;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AccountRepository extends CrudRepository<Account, Long> {

    List<Account> findAccountsByUserIdIs(Long userId);
}
