package com.demo.accountsservice.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.PositiveOrZero;

import java.math.BigDecimal;

public record AccountCashTransfer(
        @NotBlank Long userId,
        @NotBlank Long fromAccount,
        @NotBlank Long toAccount,
        @PositiveOrZero BigDecimal amount
) {
}
