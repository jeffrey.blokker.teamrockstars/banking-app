package com.demo.accountsservice.kafka.consumers;

import com.demo.accountsservice.kafka.dto.UserRegisteredEvent;
import com.demo.accountsservice.service.AccountService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class UserRegisteredConsumer {

    private static final String USER_REGISTERED_TOPIC = "users.registration.user-registered";

    private final AccountService accountService;

    @KafkaListener(topics = USER_REGISTERED_TOPIC)
    public void receiveMessage(String message) {
        try {
            UserRegisteredEvent event = new ObjectMapper().readValue(message, UserRegisteredEvent.class);

            accountService.openAccount(event.userId(), event.openingAmount());
            //process the eventId
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}
