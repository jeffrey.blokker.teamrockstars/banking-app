package com.demo.accountsservice.kafka.dto;

import java.math.BigDecimal;
import java.util.UUID;

public record UserRegisteredEvent(
        UUID eventId,
        Long userId,
        BigDecimal openingAmount) {
}
