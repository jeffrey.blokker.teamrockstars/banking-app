package com.demo.accountsservice.service;

import com.demo.accountsservice.dto.AccountCashTransfer;
import com.demo.accountsservice.model.Account;
import com.demo.accountsservice.repository.AccountRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;

@Service
@Transactional
@RequiredArgsConstructor
public class AccountService {

    private final AccountRepository accountRepository;

    public void openAccount(Long userId, BigDecimal openingAmount) {
        Account account = Account.builder()
                                 .userId(userId)
                                 .amount(openingAmount)
                                 .build();

        accountRepository.save(account);
    }

    public List<Account> findAccountsBy(Long userId) {
        return accountRepository.findAccountsByUserIdIs(userId);
    }

    public void transferCashBetweenAccounts(AccountCashTransfer transfer) {
        Account fromAccount = accountRepository.findById(transfer.fromAccount()).orElseThrow(() -> new RuntimeException("Account does not exist!"));
        Account toAccount = accountRepository.findById(transfer.toAccount()).orElseThrow(() -> new RuntimeException("Account does not exist!"));

        if(!fromAccount.getUserId().equals(transfer.userId())) {
            throw new RuntimeException("This user does not own this account!");
        }

        fromAccount.setAmount(fromAccount.getAmount().subtract(transfer.amount()));
        toAccount.setAmount(toAccount.getAmount().add(transfer.amount()));

        accountRepository.saveAll(List.of(fromAccount, toAccount));
    }
}
